/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor/es: Pilz Melina
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
void printBits(size_t const size, void const * const ptr)
{
	unsigned char *b=(unsigned char*)ptr;
	unsigned char byte;
	int i,j;

	for(i=size-1;i>=0;i--)
	{
		for(j=7;j>=0;j--)
		{
		byte=(b[i]>>j)&1;
		printf("%u",byte);
		}
	}
	puts("");
}

int main()
{
         uint32_t value=0x01020304;

         uint8_t byte_0, byte_1, byte_2, byte_3;

         printf("Valor de la variable en binario: \n");
         printBits(sizeof(value),&value);
         printf("\n");

         byte_3 = value>>24;  //TRUNCAMIENTO IMPLICITO
         byte_2 = value>>16;
         byte_1 = value>>8;
         byte_0 = value;

         printf("Byte 0: %i\n",byte_0);
         printBits(sizeof(byte_0),&byte_0);
         printf("Byte 1: %i\n",byte_1);
         printBits(sizeof(byte_1),&byte_1);
         printf("Byte 2: %i\n",byte_2);
         printBits(sizeof(byte_2),&byte_2);
         printf("Byte 3: %i\n",byte_3);
         printBits(sizeof(byte_3),&byte_3);
         printf("\n");

         union Union32_8
         {
        	 struct {
         		    	uint8_t LSB;
         		    	uint8_t B;
         		    	uint8_t C;
         		    	uint8_t MSB;
         		    }byte;
         uint32_t bit32;
         } num;

         num.bit32=0x01020304;
         printf("Usando union: \n");
         printf("Byte 0: %i\n",num.byte.LSB);
         printBits(sizeof(num.byte.LSB),&num.byte.LSB);
         printf("Byte 1: %i\n",num.byte.B);
         printBits(sizeof(num.byte.B),&num.byte.B);
         printf("Byte 2: %i\n",num.byte.C);
         printBits(sizeof(num.byte.C),&num.byte.C);
         printf("Byte 3: %i\n",num.byte.MSB);
         printBits(sizeof(num.byte.MSB),&num.byte.MSB);

         return 0;
 }


/*==================[end of file]============================================*/

