/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor/es: Pilz Melina
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/
#define BIT3 3
#define BIT5 5

/*==================[internal functions declaration]=========================*/

void printBits(size_t const size, void const * const ptr)
{
	unsigned char *b=(unsigned char*)ptr;
	unsigned char byte;
	int i,j;

	for(i=size-1;i>=0;i--)
	{
		for(j=7;j>=0;j--)
		{
		byte=(b[i]>>j)&1;
		printf("%u",byte);
		}
	}
	puts("");
}

int main(void)
{
	uint32_t Var;
	printf("La variable es: \n");
	printBits(sizeof(Var),&Var);
	uint32_t mask3=1<<BIT3;
	uint32_t mask5=1<<BIT5;  //Máscara con el bit 0 a 1

	if((Var&mask3)==(mask3))  //Si esta operación da verdadero, debo llevar el bit 3 a 0
	{
		if((Var&mask5)==(mask5))
		{
			Var=(Var&~mask5)&(~mask3);
		}
		else
		{
			Var=(Var|mask5)&(~mask3);
		}

	}
	else   //Sino debo llevar el bit 0 a 1
	{
		if((Var&mask5)==(mask5))
		{
			Var=(Var&~mask5)|(mask3);
		}
		else
		{
			Var=(Var|mask5)|(mask3);
		}

	}

	printf("\nLa variable con bit3 y bit5 invertidos es: \n");
	printBits(sizeof(Var),&Var);

	return 0;
}

/*==================[end of file]============================================*/

