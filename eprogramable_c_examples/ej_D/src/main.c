/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor/es: Pilz Melina
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/
//Escribir una función que reciba como parámetro un dígito BCD y un vector de
//estructuras del tipo  gpioConf_t.

typedef enum
{
	IN,
	OUT,
}dir_pin;

typedef enum
{
	Puerto1=1,
	Puerto2,
}num_port;

typedef enum
{
	Pin1=1,Pin2,Pin3,Pin4,Pin5,
	Pin6,Pin7,Pin8,Pin9,Pin10,
	Pin11,Pin12,Pin13,Pin14
}num_pin;


typedef struct
{
	uint8_t port;			/*!< GPIO port number */
    uint8_t pin;			/*!< GPIO pin number */
	uint8_t dir;			/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;

//Defina un vector que mapee los bits de la siguiente manera:
//b0 -> puerto 1.4
//b1 -> puerto 1.5
//b2 -> puerto 1.6
//b3 -> puerto 2.14

void BcdToLed(uint8_t bcd, gpioConf_t config[])
{
	uint8_t i=0;

	for(i=0;i<4;i++)
	{
		if (bcd&(1<<i))
		{
			printf("Puerto %d.%d 1 \r\n", config[i].port, config[i].pin);
		}
		else
		{
			printf("Puerto %d.%d 0 \r\n", config[i].port, config[i].pin);
		}
	}
}

/*==================[internal functions declaration]=========================*/

int main(void)
{
	gpioConf_t num[4]={{Puerto1,Pin4,OUT},{Puerto1,Pin5,IN},{Puerto1,Pin6,OUT},{Puerto2,Pin14,OUT}};

	BcdToLed(15,num);

return 0;
}


/*==================[end of file]============================================*/
