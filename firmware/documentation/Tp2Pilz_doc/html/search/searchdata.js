var indexSectionsWithContent =
{
  0: "_bdfghlmpst",
  1: "d",
  2: "g",
  3: "dgls",
  4: "ghmp",
  5: "gls",
  6: "g",
  7: "bdgls",
  8: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "groups",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Modules",
  8: "Pages"
};

