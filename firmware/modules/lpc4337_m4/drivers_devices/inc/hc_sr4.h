/* @brief  EDU-CIAA NXP GPIO driver
 * @author Melina Pilz
 *
 *
 * @note
 *
 *
 */

#ifndef SHC_SR4_H_
#define SHC_SR4_H_

#include "bool.h"
#include "gpio.h"

/*****************************************************************************
 * Public macros/types/enumerations/variables definitions
 ****************************************************************************/

/*****************************************************************************
 * Public functions definitions
 ****************************************************************************/

/**
 * @brief Initialize pins echo and trigger
 * @param[in]
 * @return TRUE if no error
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);


/**
 * @brief Calculate distance in Centimeters
 * @param[in] None
 * @return Distance in centimeter
 */
int16_t HcSr04ReadDistanceCentimeters(void);


/**
 * @brief Calculate distance in Inches
 * @param[in] None
 * @return Distance in inch
 */
int16_t HcSr04ReadDistanceInches(void);


/**
 * @brief Initialize device.
 * @param[in]
 * @return TRUE if no error
 */
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);


/**
 * @brief Calculate distance
 * @param[in]
 * @return NONE
 */
void HcSr04GetTimeUs(gpio_t echo, gpio_t trigger);

#endif /* SHC_SR4_H_ */
