/** @brief Header for driver for Ultrasonic Ranging Module in the EDU-CIAA board.
 **
 **
 ** @author Melina Pilz
 **/

/*==================[inclusions]=============================================*/
#include "hc_sr4.h"
#include "bool.h"
#include "delay.h"
#include "gpio.h"
#include "delay.h"

gpio_t echo_saved, trigger_saved;
uint16_t contador;
/*==================[external functions definition]==========================*/
bool HcSr04Init(gpio_t echo, gpio_t trigger)
{
	echo_saved=echo;
	trigger_saved=trigger;
	GPIOInit(echo_saved, GPIO_INPUT);
	GPIOInit(trigger_saved, GPIO_OUTPUT);

	return true;
}

void HcSr04GetTimeUs(gpio_t echo, gpio_t trigger)
{

	contador=0;

	GPIOOn(trigger);
	DelayUs(10);
	GPIOOff(trigger);


	while(GPIORead(echo)==0)
		{



		}

	while(GPIORead(echo)==1)
	{

		DelayUs(1);
		contador++;

	}


}

int16_t HcSr04ReadDistanceCentimeters(void)
{

	return (contador/27);
}

int16_t HcSr04ReadDistanceInches(void)
{

	return (contador/148);
}

bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
{
	GPIODeinit();
	return true;
}

/*==================[end of file]============================================*/
