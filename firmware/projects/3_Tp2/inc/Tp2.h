/*! @mainpage Tp2
 *
 * \section genDesc General Description
 *
 * Esta aplicación prende distintos leds cuando la distancia al sensor de ultrasonido esta entre ciertos rangos.
 *
 * <a href="https://drive.google.com/drive/folders/1pIh99S6KsvxsHuOLfFtFNPL-TJ1pxO4l?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * | Ultrasound Sensor |  EDU-CIAA	|
 * |:-----------------:|:-----------|
 * | 	  ECHO	 	   | 	T_FIL2	|
 * | 	TRIGGER	 	   | 	T_FIL3	|
 * | 	  GND 	 	   | 	GND		|
 * | 	  VCC 	 	   | 	5V		|
 *
 *
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/09/2020 | Document creation		                         |
 * | 16/09/2020	| A functionality is added	                     |
 * | 20/09/2020	| Changes in documentation                    	 |
 *
 * @author Pilz Melina
 *
 */

#ifndef _TP2_H
#define _TP2_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Función Principal
 * @return None
 */
int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _TP2_H */

