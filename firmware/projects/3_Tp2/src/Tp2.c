/*! @mainpage Tp2
 *
 * \section genDesc General Description
 *
 * Esta aplicación prende distintos leds cuando la distancia al sensor de ultrasonido esta entre ciertos rangos.
 *
 * \section hardConn Hardware Connection
 *
 * | Ultrasound Sensor |  EDU-CIAA	|
 * |:-----------------:|:-----------|
 * | 	  ECHO	 	   | 	T_FIL2	|
 * | 	TRIGGER	 	   | 	T_FIL3	|
 * | 	  GND 	 	   | 	GND		|
 * | 	  VCC 	 	   | 	5V		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/09/2020 | Document creation		                         |
 * | 20/09/2020	| Changes in documentation                    	 |
 *
 * @author Pilz Melina
 *
 */

/*==================[inclusions]=============================================*/
#include "Tp2.h" /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "hc_sr4.h"
#include "delay.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
/** @def DIST_0
*	@brief Distancia correspondiente a 0 centimetros
*/
#define DIST_0 0

/** @def DIST_10
*	@brief Distancia correspondiente a 10 centimetros
*/
#define DIST_10 10

/** @def DIST_20
*	@brief Distancia correspondiente a 20 centimetros
*/
#define DIST_20 20

/** @def DIST_30
*	@brief Distancia correspondiente a 30 centimetros
*/
#define DIST_30 30


/*==================[internal data definition]===============================*/
uint8_t ACTIVO=0; /**< Esta variable indica si TEC 1 esta pulsada */
uint8_t HOLD=0; /**< Esta variable indica si TEC 2 esta pulsada */

/*==================[internal functions declaration]=========================*/

/**
 * @brief Cambia el estado de la variable ACTIVO
 * @return None
 */
void  ISR_Tecla1()
{
	ACTIVO=!ACTIVO;
}

/**
 * @brief Cambia el estado de la variable HOLD
 * @return None
 */
void  ISR_Tecla2()
{
	HOLD=!HOLD;
}

/**
 * @brief Prende leds correspondientes a un rango de distancia entre 0 y 10cm.
 * @return None
 */
void OnRango0_10()
{
	LedOn(LED_RGB_B);
	LedOff(LED_1);
	LedOff(LED_2);
	LedOff(LED_3);
}

/**
 * @brief Prende leds correspondientes a un rango de distancia entre 10 y 20cm.
 * @return None
 */
void OnRango10_20()
{
	LedOn(LED_RGB_B);
	LedOn(LED_1);
	LedOff(LED_2);
	LedOff(LED_3);
}

/**
 * @brief Prende leds correspondientes a un rango de distancia entre 20 y 30cm.
 * @return None
 */
void OnRango20_30()
{
	LedOn(LED_RGB_B);
	LedOn(LED_1);
	LedOn(LED_2);
	LedOff(LED_3);
}

/**
 * @brief Prende leds correspondientes a una distancia mayor a 30cm.
 * @return None
 */
void OnMayor_30()
{
	LedOn(LED_RGB_B);
	LedOn(LED_1);
	LedOn(LED_2);
	LedOn(LED_3);
}

/*==================[external data definition]===============================*/
/*==================[external functions definition]==========================*/

int main(void)
{

	SystemClockInit();
	LedsInit();
	SwitchesInit();
	SwitchActivInt(SWITCH_1, ISR_Tecla1);
	SwitchActivInt(SWITCH_2, ISR_Tecla2);
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	int16_t Distance; //Variable a la que se le asigna la distancia medida

	while(1)
	{

		if(ACTIVO)
		{
			HcSr04GetTimeUs(GPIO_T_FIL2,GPIO_T_FIL3);

			Distance = HcSr04ReadDistanceCentimeters();

			if (!HOLD)
			{

				if((Distance>DIST_0)&&(Distance<=DIST_10))
				{
					OnRango0_10();

				}

				if((Distance>DIST_10)&&(Distance<=DIST_20))
				{
					OnRango10_20();

				}

				if((Distance>DIST_20)&&(Distance<=DIST_30))
				{
					OnRango20_30();

				}

				if(Distance>DIST_30)
				{
					OnMayor_30();

				}

			}
		}


	}

	return 0;
}


/*==================[end of file]============================================*/

