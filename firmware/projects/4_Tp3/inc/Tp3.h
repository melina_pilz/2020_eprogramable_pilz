/*! @mainpage Tp3
 *
 * \section genDesc General Description
 *Esta aplicación prende distintos leds cuando la distancia al sensor de ultrasonido esta entre ciertos rangos, ademas muestra la distancia por pantalla.
 *
 *
 * <a href="https://youtu.be/EC0q_zhEEgE">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * | Ultrasound Sensor |  EDU-CIAA	|
 * |:-----------------:|:-----------|
 * | 	  ECHO	 	   | 	T_FIL2	|
 * | 	TRIGGER	 	   | 	T_FIL3	|
 * | 	  GND 	 	   | 	GND		|
 * | 	  VCC 	 	   | 	5V		|
 *
 *
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 23/09/2020 | Document creation		                         |
 *
 *
 * @author Pilz Melina
 *
 */

#ifndef _TP3_H
#define _TP3_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Función Principal
 * @return None
 */
int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _TP3_H */

