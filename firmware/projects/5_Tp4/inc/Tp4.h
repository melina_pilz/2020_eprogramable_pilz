/*! @mainpage Tp4
 *
 * \section genDesc General Description
 *Esta aplicación es un osciloscopio digital.
 *
 *
 *
 * <a href="https://youtu.be/6NQ3gXJxyCA">Operation Example</a>
 *
 *
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 07/10/2020 | Document creation		                         |
 *
 *
 * @author Pilz Melina
 *
 */

#ifndef _TP4_H
#define _TP4_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Función Principal
 * @return None
 */
int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _TP4_H */

