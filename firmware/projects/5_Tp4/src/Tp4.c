/*! @mainpage Tp4
 *
 * \section genDesc General Description
 * Esta aplicación es un osciloscopio digital.
 *
 *
 * \section hardConn Hardware Connection
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 7/10/2020 	| Document creation		                         |

 *
 * @author Pilz Melina
 *
 */

/*==================[inclusions]=============================================*/
#include "Tp4.h" /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/
/** @def BUFFER_SIZE
*	@brief Tamaño vector ecg
*/
#define BUFFER_SIZE 231


/*==================[internal data definition]===============================*/
uint16_t ADC_value = 0;
uint8_t muestra = 0;
uint8_t received_data = 0;

const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

/*==================[internal functions declaration]=========================*/
/**
 * @brief Función que da funcionalidad a la tecla 1.
 * @return None
 */
void  ISR_Tecla1(void);

/**
 * @brief Función que da funcionalidad a la tecla 2.
 * @return None
 */
void  ISR_Tecla2(void);

/**
 * @brief Función que da funcionalidad a la tecla 3.
 * @return None
 */
void  ISR_Tecla3(void);

/**
 * @brief Función que da funcionalidad a la tecla 4.
 * @return None
 */
void  ISR_Tecla4(void);

/**
 * @brief Comienza la conversión.
 * @return None
 */
void doTimerA(void);

/**
 * @brief Escribe el valor correspondiente al ECG.
 * @return None
 */
void doTimerB(void);

/**
 * @brief Lee el dato recibido.
 * @return None
 */
void doUart(void);

/**
 * @brief Lee el canal correspondiente y escribe el dato via PUERTO SERIE.
 * @return None
 */
void doADC(void);

/**
 * @brief Inicializa el sistema.
 * @return None
 */
void SysInit(void);

/*==================[external data definition]==========================*/

timer_config my_timerA = {TIMER_A, 2, &doTimerA};
timer_config my_timerB = {TIMER_B, 4, &doTimerB};
serial_config my_port = {SERIAL_PORT_PC, 115200, &doUart};
analog_input_config my_ADC ={CH1, AINPUTS_SINGLE_READ, &doADC};

/*==================[internal functions declaration]==========================*/
/*==================[external functions definition]==========================*/

void  ISR_Tecla1(void)
{
}

void  ISR_Tecla2(void)
{
}

void  ISR_Tecla3(void)
{
}

void  ISR_Tecla4(void)
{
}

void doTimerA(void)
{
	AnalogStartConvertion();
}

void doTimerB(void)
{
	AnalogOutputWrite(ecg[muestra]);
	muestra ++;
	if (muestra == 231)
	{
		muestra = 0;
	}
}

void doUart(void)
{
	UartReadByte(SERIAL_PORT_PC, &received_data);
}

void doADC(void)
{
	AnalogInputRead(CH1,&ADC_value);
	UartSendString(SERIAL_PORT_PC,UartItoa(ADC_value,10));
	UartSendString(SERIAL_PORT_PC," \r");
}

void SysInit(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();

	TimerInit(&my_timerA);
	TimerInit(&my_timerB);
	TimerStart(TIMER_A);
	TimerStart(TIMER_B);

	UartInit(&my_port);

	AnalogInputInit(&my_ADC);
	AnalogOutputInit();

	SwitchActivInt(SWITCH_1, ISR_Tecla1);
	SwitchActivInt(SWITCH_2, ISR_Tecla2);
	SwitchActivInt(SWITCH_3, ISR_Tecla3);
	SwitchActivInt(SWITCH_4, ISR_Tecla4);

}

int main(void)
{
	SysInit();

	while(1)
	{
	}

	return 0;
}


/*==================[end of file]============================================*/


