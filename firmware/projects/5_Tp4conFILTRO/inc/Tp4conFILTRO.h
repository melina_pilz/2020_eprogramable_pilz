/*! @mainpage Tp4
 *
 * \section genDesc General Description
 *Esta aplicación es un osciloscopio digital con filtro.
 *
 *
 *
 * <a href="https://youtu.be/NN63yw86ZRA">Operation Example</a>
 *
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 14/10/2020 | Document creation		                         |
 *
 *
 * @author Pilz Melina
 *
 */

#ifndef _TP4_H
#define _TP4_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Función Principal
 * @return None
 */
int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _TP4_H */

