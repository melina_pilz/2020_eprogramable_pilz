/*! @mainpage Tp4
 *
 * \section genDesc General Description
 * Esta aplicación es un osciloscopio digital con filtro.
 *
 *
 * \section hardConn Hardware Connection
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 14/10/2020 	| Document creation		                         |

 *
 * @author Pilz Melina
 *
 */
/*==================[inclusions]=============================================*/
#include "Tp4conFILTRO.h" /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/
/** @def BUFFER_SIZE
*	@brief Tamaño vector ecg
*/
#define BUFFER_SIZE 231

/*==================[internal data definition]===============================*/
uint16_t ADC_value = 0;
uint8_t muestra = 0;
uint8_t received_data = 0;
bool filtrado = FALSE;
uint16_t valor_filtrado;
uint16_t valor_anterior;
uint8_t fc = 100;
float PERIODO = 0.002;
float alfa;
float RC;

const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

/*==================[internal functions declaration]=========================*/
/**
 * @brief Función que da funcionalidad a la tecla 1.
 * @return None
 */
void  ISR_Tecla1(void);

/**
 * @brief Función que da funcionalidad a la tecla 2.
 * @return None
 */
void  ISR_Tecla2(void);

/**
 * @brief Función que da funcionalidad a la tecla 3.
 * @return None
 */
void  ISR_Tecla3(void);

/**
 * @brief Función que da funcionalidad a la tecla 4.
 * @return None
 */
void  ISR_Tecla4(void);

/**
 * @brief Comienza la conversión.
 * @return None
 */
void doTimerA(void);

/**
 * @brief Escribe el valor correspondiente al ECG.
 * @return None
 */
void doTimerB(void);


/**
 * @brief Lee el canal correspondiente y escribe el dato via PUERTO SERIE.
 * @return None
 */
void doADC(void);

/**
 * @brief Filtra la señal y devuelve su valor.
 * @return Señal filtrada.
 */
uint16_t Filtro(uint16_t actual, uint16_t anterior);

/**
 * @brief Inicializa el sistema.
 * @return None
 */
void SysInit(void);

/*==================[external data definition]==========================*/

timer_config my_timerA = {TIMER_A, 2, &doTimerA};
timer_config my_timerB = {TIMER_B, 4, &doTimerB};
serial_config my_port = {SERIAL_PORT_PC, 115200, NULL};
analog_input_config my_ADC ={CH1, AINPUTS_SINGLE_READ, &doADC};

/*==================[internal functions declaration]==========================*/

uint16_t Filtro(uint16_t actual, uint16_t anterior)
{
	uint16_t salida_filtrada;
	salida_filtrada = anterior+alfa*(actual-anterior);
	return salida_filtrada;
}

void doTimerA(void)
{
	AnalogStartConvertion();
}

void doTimerB(void)
{
	AnalogOutputWrite(ecg[muestra]);
	muestra ++;
	if (muestra == 231)
	{
		muestra = 0;
	}
}

void doADC(void)
{
	AnalogInputRead(CH1,&ADC_value);
	if(filtrado)
	{
		valor_filtrado = Filtro(ADC_value ,valor_anterior);
		valor_anterior = valor_filtrado;
		UartSendString(SERIAL_PORT_PC,UartItoa(valor_filtrado,10));
		UartSendString(SERIAL_PORT_PC,",");
	}
	else
	{

	}

	UartSendString(SERIAL_PORT_PC,UartItoa(0,10));
	UartSendString(SERIAL_PORT_PC," \r");
}

void  ISR_Tecla1(void)
{
	filtrado=TRUE;
	RC = 1/(2*3.14*fc);
	alfa = PERIODO/(RC + PERIODO);
}

void  ISR_Tecla2(void)
{
	filtrado=FALSE;
}

void  ISR_Tecla3(void)
{
	if(fc>30)
	{
		fc = fc - 30;
	}
	RC = 1/(2*3.14*fc);
	alfa = PERIODO/(RC + PERIODO);
}

void  ISR_Tecla4(void)
{
	if(fc<245)
	{
		fc = fc + 30;
	}
	RC = 1/(2*3.14*fc);
	alfa = PERIODO/(RC + PERIODO);
}

void SysInit(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();

	TimerInit(&my_timerA);
	TimerInit(&my_timerB);
	TimerStart(TIMER_A);
	TimerStart(TIMER_B);

	UartInit(&my_port);

	AnalogInputInit(&my_ADC);
	AnalogOutputInit();

	SwitchActivInt(SWITCH_1, ISR_Tecla1);
	SwitchActivInt(SWITCH_2, ISR_Tecla2);
	SwitchActivInt(SWITCH_3, ISR_Tecla3);
	SwitchActivInt(SWITCH_4, ISR_Tecla4);

}

/*==================[external functions definition]==========================*/

int main(void)
{
	SysInit();

	while(1)
	{
	}

	return 0;
}


/*==================[end of file]============================================*/


