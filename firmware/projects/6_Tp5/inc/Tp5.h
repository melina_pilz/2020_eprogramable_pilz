/*! @mainpage Tp5: Proyecto individual
 *
 * \section genDesc General Description
 *Termómetro digital con timer para cocción del huevo.
 *
 *
 * <a href="https://youtu.be/3y6DkFk8ack">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Sonda NTC	 |  EDU-CIAA  |
 * |:---------------:|:-----------|
 * | 	  ECHO	 	 | 	 CH1	  |
 * | 	  GND 	 	 | 	 GNDA	  |
 * | 	  VCC 	 	 | 	 VDDA	  |
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/10/2020 | Creación de documento	                         |
 * | 25/10/2020 | Funcionalidad completa                         |
 *
 *
 * @author Pilz Melina
 *
 */

#ifndef _TP5_H
#define _TP5_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Función Principal
 * @return None
 */
int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _TP5_H */

