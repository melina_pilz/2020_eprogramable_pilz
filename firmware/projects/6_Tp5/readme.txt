﻿Descripción del Proyecto.

Esta aplicación representa el funcionamiento de un termometro digital para la cocción del huevo, teniendo en cuenta que un huevo es seguro de consumir si se cocina a mas de 65°C por al menos 3 minutos.

Para simular esto, tomamos la tempreatura cada 1 segundo simulada con la variación de resistencia de un potenciometro. Cada 3 muestras, y tomando la menor, mandamos a la UART para su visualización.

Mientras la temperatura minima sea menor a 65°C, hay un led rojo encendido.

Si esa temperatura minima sobrepasa los 65°C por al menos 15 segundos, la medición se frena y se puede ver un mensaje que nos anuncia que el huevo esta apto para el consumo, acompañado de un led verde prendido.
