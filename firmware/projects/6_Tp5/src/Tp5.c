/*! @mainpage Tp5: Proyecto individual
 *
 * \section genDesc General Description
 * Termómetro digital con timer para cocción del huevo.
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   Sonda NTC	 |  EDU-CIAA  |
 * |:---------------:|:-----------|
 * | 	  ECHO	 	 | 	 CH1	  |
 * | 	  GND 	 	 | 	 GNDA	  |
 * | 	  VCC 	 	 | 	 VDDA	  |
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/10/2020 | Creación de documento	                         |
 * | 25/10/2020 | Funcionalidad completa                         |
 *
 * @author Pilz Melina
 *
 */

/*==================[inclusions]=============================================*/
#include "Tp5.h" /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/
/** @def MUESTRAS
*	@brief Indica el tamaño vector de muestras.
*/
#define MUESTRAS 3

/*==================[internal data definition]===============================*/
enum {OFF,ON};
uint16_t ADC_value = 0; //!&lt; Guarda el valor del ADC.
uint8_t received_data = 0; //!&lt; Guarda el dato recibido del teclado.
uint16_t vector[MUESTRAS]; //!&lt; Vector que gurda las muestras de temperatura.
uint8_t n = 0; //!&lt; Indice del vector
uint8_t Estado = OFF; //!&lt; Define el estado del sistema.
uint16_t valor_procesado; //!&lt; Contiene el valor de la señal luego del procesamiento.

/**
 * @brief Tabla correspondiente a la sonda NTC.
 */
int NTC_table[256] = {
  -69, -64, -59, -55, -52, -49, -46, -44, -42,
  -41, -39, -38, -36, -35, -34, -32, -31, -30,
  -29, -28, -27, -26, -26, -25, -24, -23, -22,
  -22, -21, -20, -19, -19, -18, -17, -17, -16,
  -15, -15, -14, -14, -13, -12, -12, -11, -11,
  -10, -10, -9, -9, -8, -8, -7, -7, -6, -6,
  -5, -5, -4, -4, -3, -3, -2, -2, -1, -1, 0,
  0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 4, 5, 5, 6,
  6, 6, 7, 7, 8, 8, 9, 9, 9, 10, 10, 11, 11,
  11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 15,
  16, 16, 17, 17, 17, 18, 18, 19, 19, 19, 20,
  20, 21, 21, 21, 22, 22, 23, 23, 23, 24, 24,
  25, 25, 25, 26, 26, 27, 27, 27, 28, 28, 29,
  29, 30, 30, 30, 31, 31, 32, 32, 33, 33, 34,
  34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39,
  39, 40, 40, 41, 41, 42, 42, 43, 43, 44, 44,
  45, 45, 46, 46, 47, 47, 48, 48, 49, 50, 50,
  51, 51, 52, 53, 53, 54, 54, 55, 56, 56, 57,
  58, 58, 59, 60, 60, 61, 62, 63, 63, 64, 65,
  66, 67, 68, 68, 69, 70, 71, 72, 73, 74, 75,
  76, 77, 78, 79, 80, 82, 83, 84, 85, 87, 88,
  90, 91, 93, 94, 96, 98, 100, 102, 104, 106,
  108, 110, 113, 116, 119, 122, 125, 129, 133,
  137, 142, 148, 155, 162, 171, 182, 197, 217,
  247, 310, 373
};

/*==================[internal functions declaration]=========================*/
/**
 * @brief Función que da funcionalidad a la tecla 1.
 * @return None
 */
void  ISR_Tecla1(void);

/**
 * @brief Función que da funcionalidad a la tecla 2.
 * @return None
 */
void  ISR_Tecla2(void);


/**
 * @brief Comienzo de la conversión.
 * @return None
 */
void doTimerA(void);

/**
 * @brief Envia un aviso de cumplimiento de la temperatura y tiempo adecuados.
 * @return None
 */
void doTimerB(void);

/**
 * @brief Lee el dato recibido del teclado.
 * @return None
 */
void doUart(void);

/**
 * @brief Calcula el minimo de un vector de valores.
 * @return Valor minimo calculado.
 */
uint16_t  Minimo(uint16_t *p);

/**
 * @brief Busca en la LUT el valor de temperatura correspondiente.
 * @return Valor de temperatura
 */
uint16_t NTC_ADC2Temperature(uint16_t adc_value);

/**
 * @brief Lee el canal correspondiente y escribe el dato via PUERTO SERIE.
 * @return None
 */
void doADC(void);

/**
 * @brief Inicializa el sistema.
 * @return None
 */
void SysInit(void);

/*==================[external data definition]==========================*/
timer_config my_timerA = {TIMER_A, 1000, &doTimerA};
timer_config my_timerB = {TIMER_B, 15000, &doTimerB};
serial_config my_port = {SERIAL_PORT_PC, 9600, &doUart};
analog_input_config my_ADC ={CH1, AINPUTS_SINGLE_READ, &doADC};

/*==================[internal functions declaration]==========================*/
uint16_t NTC_ADC2Temperature(uint16_t adc_value)
{
	return NTC_table[adc_value];
}

void  ISR_Tecla1(void)
{
	Estado = ON;
}

void  ISR_Tecla2(void)
{
	Estado = OFF;
}

uint16_t  Minimo(uint16_t *p)
{
	uint8_t i;
	uint16_t min;
	for(i=0;i<=3;i++)
	{
		if(p[i]<min)
		{
			min = p[i];
		}
	}
	return min;
}

void doTimerA(void)
{
	AnalogStartConvertion();
}

void doTimerB(void)
{
	LedOn(LED_3);
	UartSendString(SERIAL_PORT_PC,"HUEVO APTO PARA CONSUMO\r\n");
	Estado=OFF;
	TimerStop(TIMER_B);
}

void doUart(void)
{
	UartReadByte(SERIAL_PORT_PC, &received_data);
	switch (received_data)
	{
		case 'A':
			Estado=OFF;
			break;

		case 'P':
			Estado=ON;
			break;
	}
}

void doADC(void)
{
	if(Estado==ON)
	{
		AnalogInputRead(CH1,&ADC_value);
		uint16_t temperatura;
		temperatura=NTC_ADC2Temperature(ADC_value);
		LedOn(LED_RGB_R);
		vector[n]=temperatura;
		if(n>3)
		{
			n=0;
		}
		else
		{
			n++;
		}
		if(n==3)
		{
			valor_procesado = Minimo(vector);
			UartSendString(SERIAL_PORT_PC,UartItoa(valor_procesado,10));
			UartSendString(SERIAL_PORT_PC," grados Celsius\r\n");
			if(valor_procesado>65)
			{
				TimerStart(TIMER_B);
			}
			else
			{
				TimerStop(TIMER_B);
			}

		}
	}
	else
	{
		LedOff(LED_RGB_R);
	}
}

void SysInit(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();

	TimerInit(&my_timerA);
	TimerInit(&my_timerB);
	TimerStart(TIMER_A);

	UartInit(&my_port);

	AnalogInputInit(&my_ADC);
	AnalogOutputInit();

	SwitchActivInt(SWITCH_1, ISR_Tecla1);
	SwitchActivInt(SWITCH_2, ISR_Tecla2);
}

/*==================[external functions definition]==========================*/


int main(void)
{
	SysInit();
	while(1)
	{
	}
	return 0;
}


/*==================[end of file]============================================*/


