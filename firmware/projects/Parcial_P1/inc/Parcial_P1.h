/*! @mainpage Parcial punto 1
 *
 * \section genDesc General Description
 * Esta aplicación mide el volumen de un vaso usando un sensor de ultrasonido.
 *
 *
 *
 * <a href="">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * | Ultrasound Sensor |  EDU-CIAA	|
 * |:-----------------:|:-----------|
 * | 	  ECHO	 	   | 	T_FIL2	|
 * | 	TRIGGER	 	   | 	T_FIL3	|
 * | 	  GND 	 	   | 	GND		|
 * | 	  VCC 	 	   | 	5V		|
 *
 *
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 2/11/2020 | Document creation		                         |
 *
 * @author Pilz Melina
 *
 */

#ifndef _ParcialP1_H
#define _ParcialP1_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Función Principal
 * @return None
 */
int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _ParcialP1_H */

