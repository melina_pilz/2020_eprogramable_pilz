/*! @mainpage Parcial punto 1
 *
 * \section genDesc General Description
 * Esta aplicación mide el volumen de un vaso usando un sensor de ultrasonido.
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Ultrasound Sensor |  EDU-CIAA	|
 * |:-----------------:|:-----------|
 * | 	  ECHO	 	   | 	T_FIL2	|
 * | 	TRIGGER	 	   | 	T_FIL3	|
 * | 	  GND 	 	   | 	GND		|
 * | 	  VCC 	 	   | 	5V		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 2/11/2020 | Document creation		                         |
 *
 *
 * @author Pilz Melina
 *
 */

/*==================[inclusions]=============================================*/
#include "Parcial_P1.h" /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "hc_sr4.h"
#include "delay.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"


/*Diseñar e implementar el firmware de un sistema de medición de volumen.
Suponga que tiene un vaso de 6 cm de diámetro y 10 cm de alto,
se coloca el sensor de distancia en la parte superior como muestra
en la figura:

Calcule a partir de la distancia medida a la superficie del agua la
cantidad de la misma que contiene el vaso. Envíe este dato 6 veces por
segundo por la UART a la PC en el siguiente formato: xx + “ cm3”
(uno por renglón). Utilice interrupciones de timer. (Para el cálculo de
volumen aproxime pi a 3,14).*/


/*==================[macros and definitions]=================================*/

/** @def BAUDRATE
*	@brief Velocidad en baudios del envio de informacion.
*/
#define BAUDRATE 115200

/** @def TIEMPO
*	@brief Tiempo cada cuanto se envia la informacion (6 VECES POR SEGUNDO).
*/
#define TIEMPO 1/6

/*==================[external data definition]===============================*/

/*==================[internal functions definition]=========================*/
/**
 * @brief Funcion que se llama cada vez que pasa el valor de tiempo.
 * @return None
 */
void doTimerA(void);

/*==================[internal data definition]===============================*/
timer_config my_timerA = {TIMER_A, TIEMPO , &doTimerA};
serial_config my_port = {SERIAL_PORT_PC, BAUDRATE, NULL};

float16_t PI= 3.14;
float16_t RADIO= 6/2;
float16_t ALTURA= 10;
float16_t VOL_TOTAL= PI*RADIO*RADIO*ALTURA; //Volumen total del vaso
float16_t Distance; //Distancia Medida
float16_t Vol_calc; //Volumen calculado

/*==================[internal functions declaration]=========================*/

void doTimerA(void)
{
	float16_t Altura;
	HcSr04GetTimeUs(GPIO_T_FIL2,GPIO_T_FIL3);
	Distance = HcSr04ReadDistanceCentimeters();

	Vol_calc= (VOL_TOTAL)-(PI*RADIO*RADIO*Distance); //Resto del volumen total, el volumen virtual

	UartSendString(SERIAL_PORT_PC,UartItoa(Vol_calc,10));
	UartSendString(SERIAL_PORT_PC," cm3\r\n");
}

/*==================[external functions definition]==========================*/

int main(void)
{

	SystemClockInit();
	SwitchesInit();

	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);

	TimerInit(&my_timerA);
	TimerStart(TIMER_A);

	UartInit(&my_port);

	AnalogInputInit(&my_ADC);
	AnalogOutputInit();


	while(1)
	{
	}

	return 0;
}


/*==================[end of file]============================================*/

