/*! @mainpage Parcial punto 2
 *
 * \section genDesc General Description
 *
 *
 *
 *
 * <a href="">Operation Example</a>
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 02/11/2020 | Document creation		                         |
 *
 *
 * @author Pilz Melina
 *
 */

#ifndef _Parcial_H
#define _Parcial_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Función Principal
 * @return None
 */
int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _Parcial_H */

