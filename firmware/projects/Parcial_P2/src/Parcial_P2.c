/*! @mainpage Parcial punto 2
 *
 * \section genDesc General Description
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 2/11/2020 	| Document creation		                         |

 *
 * @author Pilz Melina
 *
 */

/*==================[inclusions]=============================================*/
#include "Parcial_2.h" /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"


/*Digitalice la señal del conversor que mide el voltaje generado por el
punto medio de un potenciómetro conectado a la entrada CH2 de la
EDU-CIAA a una frecuencia de muestreo de 10Hz. Tome el valor promedio
cada 1 segundo y envíelo por el puerto serie a la pc en un formato de
caracteres legible por consola (uno por renglón). Se debe usar
interrupciones y timer.*/


/*==================[macros and definitions]=================================*/
/** @def BAUDRATE
*	@brief Velocidad en baudios de la comunicación serie.
*/
#define BAUDRATE 115200

/** @def TIEMPO
*	@brief Tiempo correspondiente a una frecuencia de 10Hz.
*/
#define TIEMPO 100

/*==================[internal data definition]===============================*/
uint16_t ADC_value = 0; // Valor del ADC
uint32_t prom_calculado=0;
uint16_t muestras=0;
uint16_t acum=0;


/*==================[internal functions declaration]=========================*/
/**
 * @brief Comienza la conversión.
 * @return None
 */
void doTimerA(void);

/**
 * @brief Lee el canal correspondiente y escribe el dato via PUERTO SERIE.
 * @return None
 */
void doADC(void);

/**
 * @brief Inicializa el sistema.
 * @return None
 */
void SysInit(void);

/*==================[external data definition]==========================*/

timer_config my_timerA = {TIMER_A, TIEMPO, &doTimerA};
serial_config my_port = {SERIAL_PORT_PC, BAUDRATE, NULL};
analog_input_config my_ADC ={CH2, AINPUTS_SINGLE_READ, &doADC};

/*==================[internal functions declaration]==========================*/
void doTimerA(void)
{
	AnalogStartConvertion();
}

void doADC(void)
{
	AnalogInputRead(CH2,&ADC_value);
	if(muestras<10)
	{
		acum+=ADC_value;
	}
	else
	{
		prom_calculado=acum/10;

		UartSendString(SERIAL_PORT_PC,UartItoa(prom_calculado,10));
		UartSendString(SERIAL_PORT_PC,"\r\n");
		muestras=0;
	}
	muestras++;
}

void SysInit(void)
{
	SystemClockInit();

	TimerInit(&my_timerA);
	TimerStart(TIMER_A);

	UartInit(&my_port);

	AnalogInputInit(&my_ADC);
	AnalogOutputInit();
}

/*==================[external functions definition]==========================*/

int main(void)
{
	SysInit();

	while(1)
	{
	}

	return 0;
}


/*==================[end of file]============================================*/


